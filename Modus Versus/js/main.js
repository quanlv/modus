(function ($) {
	"use strict";

    jQuery(document).ready(function($){
        //For Knob
        $(".skill").knob();
        
        //For Clients Carousel
        $(".clients_carousel_imgs").owlCarousel({
            items:6,
            loop:true,
            margin:30
        });
        
        var owl = $('.clients_carousel_imgs');
        owl.owlCarousel();
        // Go to the next item
        $('.fa.fa-chevron-right').click(function () {
                owl.trigger('next.owl.carousel', [500]);
            })
            // Go to the previous item
        $('.fa.fa-chevron-left').click(function () {
            // With optional speed parameter
            // Parameters has to be in square bracket '[]'
            owl.trigger('prev.owl.carousel', [500]);
        })
        
    });


    jQuery(window).load(function(){

        
    });


}(jQuery));	